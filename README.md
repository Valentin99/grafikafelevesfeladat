# Grafika Féléves feladat

## Személyes Adatok:

Név:        Barta Valentin
Neptun:     CLPE25

## Feladat leírása:
Féléves feladatom témája egy Park szimulátor lenne. Azért erre a témára esett a választásom,
mert nagyon szeretem a természetet szép kerteket és parkokat. Az félévés feladatomban megtalálható
a minimális elvárási követelményeket. ( Kamerakezelés, Objektumok, Animáció, Mozgatás Fények, Help ).
Továbbá szeretnék felhasználni benne többlet funkciókat is. ( Bonyolultabb
animációk, köd, átlátszóság, és ütközés vizsgálat.) A feladatomban
megszeretném jeleníteni egy park alaptábláját, fákat, padokat, egy
asztalt, és egy ufónak a lebegését a levegőben és egy labda
gurulását a földön. A modellek felhasználói gombnyomásra mozgathatók, továbbá a köd és átlátszóság 2 féle intenzitással állítható. Az ütközésvizsgálat az összes modell környezetében alkalmazható.


## Program futtatásához szükséges teendők:

Grafika Sdk letöltése:

```bat
https://www.uni-miskolc.hu/~matip/grafika/pages/gyak_1.html
```
Piller Imre tanár úr oldaláról a grafika_sdk.zip linkre kattintva letölthető tömöritett formában a Grafika Sdk. Letöltés után szükséges kitörömiteni az sdk-t tetszőleges helyre.
A demo mappa tözsébe bekell másolni a gitlab repositoryból letöltött projektett.

Letöltési lehetőségek:

GitBash használatával:

```bat
git clone https://gitlab.com/Valentin99/grafikabeadando.git
```

Más különben a projekt letölthető akár zip formátumba is. A download ikonra kattintva.


## Exe file létrehozása:

A grafika SDK ban található shell.bat file-ra kattintva megjelenik egy parancssor. A következő utasitásokat kell kiadni az exe file létrehozásához.

```bat
cd demo
```

```bat
cd beadando
```

```bat
make
```

Továbbá megadható az exe file létrehozó parancs gcc formátumban is.

Windows:
```bat
gcc -Iinclude src/*   -lSOIL -lglu32 -lopengl32 -lglut32 -lm -o beadando.exe
```

Linux:
```bat
gcc -Iinclude src/*  -lSOIL -lGL -lGLU -lglut -lm -o beadando.exe 
```

## Assets mappa (objektumok és texturák)
```bat
https://drive.google.com/drive/folders/17ukXIFDaLBh3tY5BNlnE-f95Pn5ndcRT?usp=sharing
```

Használata:


A google drivban található assets mappát lekell tölteni tömöritett formátumban. Letöltés után kikell csomagolni a mappát a beadando mappa törzsébe, ahol az exe file is található.



