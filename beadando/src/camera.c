#include <GL/glut.h>

#include <math.h>

#include "camera.h"
#include "draw.h"

#define M_PI 3.14159265358979323846
#define skybox_size 10000
#define small_object_size 90
#define big_object_size 100

double degree_to_radian(double degree)
{
	return degree * M_PI / 180.0;
}

void init_camera(struct Camera *camera)
{
	camera->position.x = 3000;
	camera->position.y = -1250;
	camera->position.z = 75;

	camera->pose.x = 0;
	camera->pose.y = 0;
	camera->pose.z = 90;
}

void collision_control(struct Camera *camera, Move move)
{
	if (camera->position.z < -70 )
	{
		move_camera_up(camera, 50.0);
	}

	if (camera->position.x < -skybox_size || camera->position.x > skybox_size || camera->position.y < -skybox_size || camera->position.y > skybox_size ||
		camera->position.z < -skybox_size || camera->position.z > skybox_size)
	{
		move_camera_backward(camera, 50.0);
	}

	//Kis Objektumok

	//Labda
	if (camera->position.x < 3200 + small_object_size && camera->position.x > 3200 - small_object_size && camera->position.y < -650 + small_object_size && camera->position.y > -650 - small_object_size && camera->position.z < -92 + small_object_size && camera->position.z > -92 - small_object_size)
	{
		move_camera_backward(camera, 50.0);
	}

	//Kis fa
	if (camera->position.x < 3000 + small_object_size && camera->position.x > 3000 - small_object_size && camera->position.y < -380 + small_object_size && camera->position.y > -380 - small_object_size && camera->position.z < -92 + small_object_size && camera->position.z > -92 - small_object_size)
	{
		move_camera_backward(camera, 50.0);
	}

	if (camera->position.x < 2700 + small_object_size && camera->position.x > 2700 - small_object_size && camera->position.y < -780 + small_object_size && camera->position.y > -780 - small_object_size && camera->position.z < -92 + small_object_size && camera->position.z > -92 - small_object_size)
	{
		move_camera_backward(camera, 50.0);
	}

	
	if (camera->position.x < 3300 + small_object_size && camera->position.x > 3300 - small_object_size && camera->position.y < -530 + small_object_size && camera->position.y > -530 - small_object_size && camera->position.z < -92 + small_object_size && camera->position.z > -92 - small_object_size)
	{
		move_camera_backward(camera, 50.0);
	}

	
	if (camera->position.x < 3800 + small_object_size && camera->position.x > 3800 - small_object_size && camera->position.y < -740 + small_object_size && camera->position.y > -740 - small_object_size && camera->position.z < -92 + small_object_size && camera->position.z > -92 - small_object_size)
	{
		move_camera_backward(camera, 50.0);
	}

	
	if (camera->position.x < 3400 + small_object_size && camera->position.x > 3400 - small_object_size && camera->position.y < -700 + small_object_size && camera->position.y > -700 - small_object_size && camera->position.z < -92 + small_object_size && camera->position.z > -92 - small_object_size)
	{
		move_camera_backward(camera, 50.0);
	}
	
	//Nagy objektumok

	//Nagy fa
	if (camera->position.x < 3075 + big_object_size && camera->position.x > 3075 - big_object_size && camera->position.y < -930 + big_object_size && camera->position.y > -930 - big_object_size && camera->position.z < -90 + big_object_size + 200 && camera->position.z > -90 - big_object_size + 200)
	{
		move_camera_backward(camera, 50.0);
	}

	if (camera->position.x < 3475 + big_object_size && camera->position.x > 3475 - big_object_size && camera->position.y < -930 + big_object_size && camera->position.y > -930 - big_object_size && camera->position.z < -90 + big_object_size + 200 && camera->position.z > -90 - big_object_size + 200)
	{
		move_camera_backward(camera, 50.0);
	}

	if (camera->position.x < 3675 + big_object_size && camera->position.x > 3675 - big_object_size && camera->position.y < -580 + big_object_size && camera->position.y > -580 - big_object_size && camera->position.z < -90 + big_object_size + 200 && camera->position.z > -90 - big_object_size + 200)
	{
		move_camera_backward(camera, 50.0);
	}

	if (camera->position.x < 2840 + big_object_size && camera->position.x > 2840 - big_object_size && camera->position.y < -1000 + big_object_size && camera->position.y > -1000 - big_object_size && camera->position.z < -90 + big_object_size + 200 && camera->position.z > -90 - big_object_size + 200)
	{
		move_camera_backward(camera, 50.0);
	}

	if (camera->position.x < 3275 + big_object_size && camera->position.x > 3275 - big_object_size && camera->position.y < -1000 + big_object_size && camera->position.y > -1000 - big_object_size && camera->position.z < -90 + big_object_size + 200 && camera->position.z > -90 - big_object_size + 200)
	{
		move_camera_backward(camera, 50.0);
	}

	//Asztal
	if (camera->position.x < 3450 + big_object_size && camera->position.x > 3450 - big_object_size && camera->position.y < -390 + big_object_size && camera->position.y > -390 - big_object_size && camera->position.z < -92 + big_object_size && camera->position.z > -92 - big_object_size)
	{
		move_camera_backward(camera, 50.0);
	}

	//Pad
	if (camera->position.x < 3250 + big_object_size && camera->position.x > 3250 - big_object_size && camera->position.y < -365 + big_object_size && camera->position.y > -365 - big_object_size && camera->position.z < -90 + big_object_size && camera->position.z > -90 - big_object_size)
	{
		move_camera_backward(camera, 50.0);
	}

	//Ufo
	if (camera->position.x < 2500 + big_object_size && camera->position.x > 2500 - big_object_size && camera->position.y < 0 + big_object_size && camera->position.y > 0 - big_object_size && camera->position.z < 100 + big_object_size && camera->position.z > 100 - big_object_size)
	{
		move_camera_backward(camera, 50.0);
	}
}

void set_view_point(const struct Camera *camera)
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glRotatef(-(camera->pose.x + 90), 1.0, 0, 0);
	glRotatef(-(camera->pose.z - 90), 0, 0, 1.0);
	glTranslatef(-camera->position.x, -camera->position.y, -camera->position.z);
}

void rotate_camera(struct Camera *camera, double horizontal, double vertical)
{
	camera->pose.z += horizontal / CAMERA_SPEED;
	camera->pose.x += vertical / CAMERA_SPEED;

	if (camera->pose.z < 0)
	{
		camera->pose.z += 360.0;
	}

	if (camera->pose.z > 360.0)
	{
		camera->pose.z -= 360.0;
	}

	if (camera->pose.x < 0)
	{
		camera->pose.x += 360.0;
	}

	if (camera->pose.x > 360.0)
	{
		camera->pose.x -= 360.0;
	}
}

void move_camera_up(struct Camera *camera, double distance)
{
	camera->prev_position = camera->position;
	camera->position.z += distance;
}

void move_camera_down(struct Camera *camera, double distance)
{
	camera->prev_position = camera->position;
	camera->position.z -= distance;
}

void move_camera_backward(struct Camera *camera, double distance)
{
	camera->prev_position = camera->position;
	double angle = degree_to_radian(camera->pose.z);

	camera->position.x -= cos(angle) * distance;
	camera->position.y -= sin(angle) * distance;
}

void move_camera_forward(struct Camera *camera, double distance)
{
	camera->prev_position = camera->position;
	double angle = degree_to_radian(camera->pose.z);

	camera->position.x += cos(angle) * distance;
	camera->position.y += sin(angle) * distance;
}

void step_camera_right(struct Camera *camera, double distance)
{
	camera->prev_position = camera->position;
	double angle = degree_to_radian(camera->pose.z + 90.0);

	camera->position.x -= cos(angle) * distance;
	camera->position.y -= sin(angle) * distance;
}

void step_camera_left(struct Camera *camera, double distance)
{
	camera->prev_position = camera->position;
	double angle = degree_to_radian(camera->pose.z - 90.0);

	camera->position.x -= cos(angle) * distance;
	camera->position.y -= sin(angle) * distance;
}
