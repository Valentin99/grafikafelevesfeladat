#include <GL/glut.h>
#include "SOIL/SOIL.h"

#include "callbacks.h"
#include <sys/time.h>
#include <action.h>
#include "camera.h"

int WINDOW_WIDTH;
int WINDOW_HEIGHT;

/*Models*/
SkyBox skyBox;
Object object;
MoveableObject moveableObject;
typedef GLubyte Pixel;
int help, help_on = 0;

/*Camera display*/
struct Camera camera;
float display_speed = 10;
double elapsed_time;
int previous_time;

struct Action action;

/*Ball and Ufo Move*/
Move move;
GLboolean call_moveable = FALSE;

/*Rotation of objects*/
Rotate rotate;
GLboolean rotate_objects = GL_FALSE;

/*Fog visible*/
int visibleCounter = 0;
int fogsets = 0;

GLfloat light_position[] = {1, 1, 1, 0};
GLfloat light_ambient[] = {0.5, 0.5, 0.5, 0};
GLfloat light_diffuse[] = {0.5, 0.5, 0, 0};
GLfloat light_specular[] = {1, 1, 1, 0};

void set_callbacks()
{
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(key_handler);
	glutKeyboardUpFunc(key_up_handler);
	glutMouseFunc(mouse_handler);
	glutMotionFunc(motion_handler);
	glutIdleFunc(idle);
	glutSpecialFunc(specialFunc);
}

void initialize()
{
	set_callbacks();
	init_camera(&camera);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_NORMALIZE);
	glEnable(GL_AUTO_NORMAL);
	glClearColor(0.1, 0.1, 0.1, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	glClearDepth(1.0);
	help = load_texture("assets//textures//help.png");
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, light_ambient);
	glEnable(GL_LIGHTING);
	init_entities(&skyBox, &object, &moveableObject);
	glEnable(GL_TEXTURE_2D);
}

void specialFunc(int key, int x, int y)
{
	switch (key)
	{
	case GLUT_KEY_F1:
		if (help_on)
		{
			help_on = 0;

			glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, light_ambient);
			glLightModelfv(GL_LIGHT_MODEL_AMBIENT, light_ambient);
		}
		else
		{
			help_on = 1;
			GLfloat ones[] = {1, 1, 1, 1};
			glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ones);
			glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ones);
		}
	}
}

void draw_help()
{

	GLfloat ones[] = {1, 1, 1, 1};
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ones);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ones);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, help);
	glBegin(GL_QUADS);
	glTexCoord2f(0, 0);
	glVertex3f(0, 0, 0);
	glTexCoord2f(1, 0);
	glVertex3f(WINDOW_WIDTH, 0, 0);
	glTexCoord2f(1, 1);
	glVertex3f(WINDOW_WIDTH, WINDOW_HEIGHT, 0);
	glTexCoord2f(0, 1);
	glVertex3f(0, WINDOW_HEIGHT, 0);
	glEnd();
	glDisable(GL_TEXTURE_2D);
	reshape(WINDOW_WIDTH, WINDOW_HEIGHT);
	glutSwapBuffers();
}

void update_camera_position(struct Camera *camera, double elapsed_time)
{
	double distance;

	distance = elapsed_time * MOVE_SPEED * display_speed;

	if (action.move_forward == TRUE)
	{
		move_camera_forward(camera, distance);
	}

	if (action.move_backward == TRUE)
	{
		move_camera_backward(camera, distance);
	}

	if (action.step_left == TRUE)
	{
		step_camera_left(camera, distance);
	}

	if (action.step_right == TRUE)
	{
		step_camera_right(camera, distance);
	}

	if (action.move_up == TRUE)
	{
		move_camera_up(camera, distance);
	}

	if (action.move_down == TRUE)
	{
		move_camera_down(camera, distance);
	}

	if (action.increase_light == TRUE)
	{
		if (light_ambient[0] < 1)
			light_ambient[0] = light_ambient[1] = light_ambient[2] += 0.01;
	}

	if (action.decrease_light == TRUE)
	{
		if (light_ambient[0] > -0.51)
			light_ambient[0] = light_ambient[1] = light_ambient[2] -= 0.01;
	}

	collision_control(camera, move);
}

void display()
{
	if (!help_on)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		elapsed_time = calc_elapsed_time();
		update_camera_position(&camera, elapsed_time);
		set_view_point(&camera);

		glLightfv(GL_LIGHT1, GL_POSITION, light_position);
		glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient);
		glLightfv(GL_LIGHT1, GL_SPECULAR, light_specular);
		glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, light_ambient);
		glEnable(GL_LIGHT1);

		if (visibleCounter == 1)
		{
			glPushAttrib(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_COLOR, GL_ONE_MINUS_SRC_COLOR);
			glDepthMask(GL_FALSE);
			draw_environment(skyBox, object, moveableObject, &rotate, move);
			glPopAttrib();
		}
		else if (visibleCounter == 2)
		{
			draw_skybox(skyBox);
		}
		else
		{
			draw_environment(skyBox, object, moveableObject, &rotate, move);
		}

		movement_of_objects(&move);
		rotation_of_objects(&rotate);
		reshape(WINDOW_WIDTH, WINDOW_HEIGHT);
		glutSwapBuffers();
	}
	else
	{
		draw_help();
	}
}

void reshape(GLsizei width, GLsizei height)
{
	WINDOW_WIDTH = width;
	WINDOW_HEIGHT = height;

	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (!help_on)
	{
		gluPerspective(50.0, (GLdouble)width / (GLdouble)height, 0.1, 20000.0);
	}
	else
	{
		gluOrtho2D(0, width, height, 0);
	}
}

GLuint load_texture(const char *filename)
{
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	GLuint texture_name;
	Pixel *image;
	glGenTextures(1, &texture_name);

	int width;
	int height;

	image = (Pixel *)SOIL_load_image(filename, &width, &height, 0, SOIL_LOAD_RGBA);

	glBindTexture(GL_TEXTURE_2D, texture_name);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, (Pixel *)image);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glEnable(GL_TEXTURE_2D);
	SOIL_free_image_data(image);
	return texture_name;
}

void idle()
{
	glutPostRedisplay();
}

void fog()
{
	float fogColor[] = {0.3, 0.3, 0.3};
	if (0 < fogsets)
	{
		glEnable(GL_FOG);

		glHint(GL_FOG_HINT, GL_NICEST);
		if (1 == fogsets)
		{
			glFogi(GL_FOG_MODE, GL_EXP);
		}
		if (2 == fogsets)
		{
			glFogi(GL_FOG_MODE, GL_EXP2);
		}
		glFogf(GL_FOG_DENSITY, 0.001);
		glFogf(GL_FOG_START, 1);
		glFogf(GL_FOG_END, 1000.0);
		glFogfv(GL_FOG_COLOR, fogColor);
	}
	else
	{
		glDisable(GL_FOG);
	}
}

void mouse_handler(int button, int state, int x, int y)
{
	mouse_position.mouse_x = x;
	mouse_position.mouse_y = y;
}

void motion_handler(int x, int y)
{
	double horizontal, vertical;

	horizontal = mouse_position.mouse_x - x;
	vertical = mouse_position.mouse_y - y;

	rotate_camera(&camera, horizontal, vertical);

	mouse_position.mouse_x = x;
	mouse_position.mouse_y = y;

	glutPostRedisplay();
}

double calc_elapsed_time()
{
	int current_time;
	double elapsed_time;

	current_time = glutGet(GLUT_ELAPSED_TIME);
	elapsed_time = (double)(current_time - previous_time) / 1000.0;
	previous_time = current_time;

	return elapsed_time;
}

void movement_of_objects(Move *move)
{

	float moveable_object_speed = 0;
	double distance = 0;

	if (call_moveable == FALSE)
	{
		move->ball.x = 2900;
		move->ball.y = -650;
		move->ball.z = -90;
		move->ufo.x = 2500;
		move->ufo.y = 0;
		move->ufo.z = 100;
	}

	if (call_moveable == TRUE)
	{

		distance = (4000 - move->ball.x);
		distance = (3500 - move->ufo.x);

		moveable_object_speed = ((distance / elapsed_time) / MOVE_SPEED) / 100;

		move->ball.x += moveable_object_speed;
		move->ufo.x += moveable_object_speed;

		if (move->ball.x <= 3000 && move->ufo.x == 2800)
		{
			call_moveable == FALSE;
		}
	}
}

void rotation_of_objects(Rotate *rotate)
{
	if (rotate_objects == TRUE)
	{
		rotate->ball_rotation += 3;
	}

	if (rotate_objects == TRUE)
	{
		rotate->ufo_rotation += 1;
	}
}

void key_handler(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 'w':
	case 'W':
		action.move_forward = TRUE;
		break;
	case 's':
	case 'S':
		action.move_backward = TRUE;
		break;
	case 'a':
	case 'A':
		action.step_left = TRUE;
		break;
	case 'd':
	case 'D':
		action.step_right = TRUE;
		break;
	case 'c':
	case 'C':
		action.move_down = TRUE;
		break;
	case 32:
		action.move_up = TRUE;
		break;
	case 'g':
	case 'G':
		if (visibleCounter == 2)
		{
			visibleCounter = 0;
		}
		else
		{
			visibleCounter++;
		}
		break;
	case 'q':
	case 'Q':
		rotate_objects = !rotate_objects;
		break;
	case 'r':
	case 'R':
		call_moveable = !call_moveable;
		break;
	case 'f':
	case 'F':
		if (2 == fogsets)
		{
			fogsets = 0;
			fog();
		}
		else
		{
			fogsets++;
			fog();
		}
		break;
	case 't':
	case 'T':
		glutFullScreen();
		break;
	case 'z':
	case 'Z':
		glutReshapeWindow(960, 640);
		break;
	case '+':
		action.increase_light = TRUE;
		break;
	case '-':
		action.decrease_light = TRUE;
		break;
	case 27:
		exit(0);
	}

	glutPostRedisplay();
}

void key_up_handler(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 'w':
	case 'W':
		action.move_forward = FALSE;
		break;
	case 's':
	case 'S':
		action.move_backward = FALSE;
		break;
	case 'a':
	case 'A':
		action.step_left = FALSE;
		break;
	case 'd':
	case 'D':
		action.step_right = FALSE;
		break;
	case 'c':
	case 'C':
		action.move_down = FALSE;
		break;
	case 32:
		action.move_up = FALSE;
		break;
	case '+':
		action.increase_light = FALSE;
		break;
	case '-':
		action.decrease_light = FALSE;
		break;
	case 27:
		exit(0);
	}

	glutPostRedisplay();
}