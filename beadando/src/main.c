#include <GL/glut.h>
#include "SOIL/SOIL.h"

#include <stdlib.h>
#include <stdio.h>

#include "action.h"
#include "callbacks.h"

struct Action action;

int main(int argc, char **argv)
{

	glutInit(&argc, argv);
	glutInitWindowSize(960, 640);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	int wnd = glutCreateWindow("Park Simulator");
	glutSetWindow(wnd);
	initialize();

	action.move_forward = FALSE;
	action.move_backward = FALSE;
	action.step_left = FALSE;
	action.step_right = FALSE;

	glutMainLoop();
	return 0;
}
