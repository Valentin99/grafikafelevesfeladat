#include "draw.h"
#include <GL/glut.h>
#include <stdio.h>
#include <math.h>
#include "camera.h"

void draw_triangles(const Model *model)
{
    int i, k;
    int vertex_index, normal_index;
    double x, y, z, normal_x, normal_y, normal_z;

    glBegin(GL_TRIANGLES);

    for (i = 0; i < model->n_triangles; ++i)
    {
        for (k = 0; k < 3; ++k)
        {
            normal_index = model->triangles[i].points[k].normal_index;
            normal_x = model->normals[normal_index].x;
            normal_y = model->normals[normal_index].y;
            normal_z = model->normals[normal_index].z;
            glNormal3d(normal_x, normal_y, normal_z);
            vertex_index = model->triangles[i].points[k].vertex_index;
            x = model->vertices[vertex_index].x;
            y = model->vertices[vertex_index].y;
            z = model->vertices[vertex_index].z;
            switch (k)
            {
            case 0:
                glTexCoord2f(0, 0);
                break;
            case 1:
                glTexCoord2f(0.1 * z, 0);
                break;
            case 2:
                glTexCoord2f(0, 0.01);
                break;
            }
            glVertex3d(x, y, z);
        }
    }

    glEnd();
}

void draw_quads(const Model *model)
{
    int i, k;
    int vertex_index, texture_index;
    double x, y, z, u, v;

    glBegin(GL_QUADS);

    for (i = 0; i < model->n_quads; ++i)
    {
        for (k = 0; k < 4; ++k)
        {

            texture_index = model->quads[i].points[k].texture_index;
            u = model->texture_vertices[texture_index].u;
            v = model->texture_vertices[texture_index].v;
            glTexCoord2f(u, 1 - v);

            vertex_index = model->quads[i].points[k].vertex_index;
            x = model->vertices[vertex_index].x;
            y = model->vertices[vertex_index].y;
            z = model->vertices[vertex_index].z;
            glVertex3d(x, y, z);
        }
    }

    glEnd();
}

void draw_normals(const Model *model, double length)
{
    int i;
    double x1, y1, z1, x2, y2, z2;

    glColor3f(0, 0, 1);

    glBegin(GL_LINES);

    for (i = 0; i < model->n_vertices; ++i)
    {
        x1 = model->vertices[i].x;
        y1 = model->vertices[i].y;
        z1 = model->vertices[i].z;
        x2 = x1 + model->normals[i].x * length;
        y2 = y1 + model->normals[i].y * length;
        z2 = z1 + model->normals[i].z * length;
        glVertex3d(x1, y1, z1);
        glVertex3d(x2, y2, z2);
    }
    glEnd();
}

void draw_model(const Model *model)
{
    draw_triangles(model);
    draw_quads(model);
}

void draw_environment(SkyBox SkyBox, Object Object, MoveableObject MoveableObject, Rotate *rotate, Move move)
{
    glEnable(GL_TEXTURE_2D);

    draw_skybox_bottom(SkyBox.skybox);

    draw_skybox_top(SkyBox.skybox);

    //talaj
    glPushMatrix();

    glTranslatef(0, 0, 0);
    glMaterialfv(GL_FRONT, GL_AMBIENT, Object.grass.material_ambient);
    glBindTexture(GL_TEXTURE_2D, Object.grass.texture);
    glTranslatef(3200, -650, -105);
    glScalef(1.0f, 1.0f, 1.0f);
    glRotatef(0, 0, 1, 0);
    draw_model(&Object.grass.model);

    glPopMatrix();

    //nagy fa1
    glPushMatrix();

    glTranslatef(3075, -930, -95);
    glMaterialfv(GL_FRONT, GL_AMBIENT, Object.tree.material_ambient);
    glBindTexture(GL_TEXTURE_2D, Object.tree.texture);
    glScalef(1.0f, 1.0f, 1.0f);
    glRotatef(90, 1, 0, 0);
    draw_model(&Object.tree.model);

    glPopMatrix();

    //nagy fa2
    glPushMatrix();

    glTranslatef(3475, -930, -95);
    glMaterialfv(GL_FRONT, GL_AMBIENT, Object.tree.material_ambient);
    glBindTexture(GL_TEXTURE_2D, Object.tree.texture);
    glScalef(1.0f, 1.0f, 1.0f);
    glRotatef(90, 1, 0, 0);
    draw_model(&Object.tree.model);

    glPopMatrix();

    
    //nagy fa4
    glPushMatrix();

    glTranslatef(3675, -580, -95);
    glMaterialfv(GL_FRONT, GL_AMBIENT, Object.tree.material_ambient);
    glBindTexture(GL_TEXTURE_2D, Object.tree.texture);
    glScalef(1.0f, 1.0f, 1.0f);
    glRotatef(90, 1, 0, 0);
    draw_model(&Object.tree.model);

    glPopMatrix();

    //nagy fa5
    glPushMatrix();

    glTranslatef(2840, -1000, -95);
    glMaterialfv(GL_FRONT, GL_AMBIENT, Object.tree.material_ambient);
    glBindTexture(GL_TEXTURE_2D, Object.tree.texture);
    glScalef(1.0f, 1.0f, 1.0f);
    glRotatef(90, 1, 0, 0);
    draw_model(&Object.tree.model);

    glPopMatrix();

    //nagy fa4
    glPushMatrix();

    glTranslatef(3275, -1000, -95);
    glMaterialfv(GL_FRONT, GL_AMBIENT, Object.tree.material_ambient);
    glBindTexture(GL_TEXTURE_2D, Object.tree.texture);
    glScalef(1.0f, 1.0f, 1.0f);
    glRotatef(90, 1, 0, 0);
    draw_model(&Object.tree.model);

    glPopMatrix();

    //labda
    glPushMatrix();

    glTranslatef(move.ball.x, move.ball.y, move.ball.z);
    glMaterialfv(GL_FRONT, GL_AMBIENT, MoveableObject.ball.material_ambient);
    glBindTexture(GL_TEXTURE_2D, MoveableObject.ball.texture);
    glScalef(1.0f, 1.0f, 1.0f);
    glRotatef(90, 0, 1, 0);
    glRotatef(rotate->ball_rotation, 0, 1, 0);
    draw_model(&MoveableObject.ball.model);

    glPopMatrix();

    //ufo
    glPushMatrix();

    glTranslatef(move.ufo.x, move.ufo.y, move.ufo.z);
    glMaterialfv(GL_FRONT, GL_AMBIENT, MoveableObject.ufo.material_ambient);
    glBindTexture(GL_TEXTURE_2D, MoveableObject.ufo.texture);
    glScalef(1.0f, 1.0f, 1.0f);
    glRotatef(rotate->ufo_rotation, 0, 0, 1);
    draw_model(&MoveableObject.ufo.model);

    glPopMatrix();

    //kis fa1
    glPushMatrix();

    glTranslatef(3000, -380, -95);
    glMaterialfv(GL_FRONT, GL_AMBIENT, Object.smalltree.material_ambient);
    glBindTexture(GL_TEXTURE_2D, Object.smalltree.texture);
    glScalef(1.0f, 1.0f, 1.0f);
    glRotatef(90, 1, 0, 0);
    draw_model(&Object.smalltree.model);
    
    glPopMatrix();

    //kis fa2
    glPushMatrix();

    glTranslatef(2700, -780, -95);
    glMaterialfv(GL_FRONT, GL_AMBIENT, Object.smalltree.material_ambient);
    glBindTexture(GL_TEXTURE_2D, Object.smalltree.texture);
    glScalef(1.0f, 1.0f, 1.0f);
    glRotatef(90, 1, 0, 0);
    draw_model(&Object.smalltree.model);
    
    glPopMatrix();

    //kis fa3
    glPushMatrix();

    glTranslatef(3300, -530, -95);
    glMaterialfv(GL_FRONT, GL_AMBIENT, Object.smalltree.material_ambient);
    glBindTexture(GL_TEXTURE_2D, Object.smalltree.texture);
    glScalef(1.0f, 1.0f, 1.0f);
    glRotatef(90, 1, 0, 0);
    draw_model(&Object.smalltree.model);
    
    glPopMatrix();

    //kis fa4
    glPushMatrix();

    glTranslatef(3300, -740, -95);
    glMaterialfv(GL_FRONT, GL_AMBIENT, Object.smalltree.material_ambient);
    glBindTexture(GL_TEXTURE_2D, Object.smalltree.texture);
    glScalef(1.0f, 1.0f, 1.0f);
    glRotatef(90, 1, 0, 0);
    draw_model(&Object.smalltree.model);
    
    glPopMatrix();

    //kis fa5
    glPushMatrix();

    glTranslatef(3400, -700, -95);
    glMaterialfv(GL_FRONT, GL_AMBIENT, Object.smalltree.material_ambient);
    glBindTexture(GL_TEXTURE_2D, Object.smalltree.texture);
    glScalef(1.0f, 1.0f, 1.0f);
    glRotatef(90, 1, 0, 0);
    draw_model(&Object.smalltree.model);
    
    glPopMatrix();

    //asztal
    glPushMatrix();

    glTranslatef(3450, -390, -95);
    glMaterialfv(GL_FRONT, GL_AMBIENT, Object.table.material_ambient);
    glBindTexture(GL_TEXTURE_2D, Object.table.texture);
    glScalef(1.0f, 1.0f, 1.0f);
    glRotatef(90, 1, 0, 0);
    draw_model(&Object.table.model);

    glPopMatrix();

    //pad
    glPushMatrix();

    glTranslatef(3250, -365, -95);
    glMaterialfv(GL_FRONT, GL_AMBIENT, Object.bench.material_ambient);
    glBindTexture(GL_TEXTURE_2D, Object.bench.texture);
    glScalef(1.0f, 1.0f, 1.0f);
    glRotatef(0, 1, 0, 0);
    draw_model(&Object.bench.model);

    glPopMatrix();
}
