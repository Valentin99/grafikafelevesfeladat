#ifndef SKYBOX_H_
#define SKYBOX_H_
#include <stdio.h>
#include "entity.h"

#define SKYBOX_SIZE 10000.0

typedef struct
{
    Entity skybox;
} SkyBox;

void draw_skybox(SkyBox SkyBox);

void draw_skybox_top(Entity skybox);

void draw_skybox_bottom(Entity skybox);

#endif