#ifndef MOVE_H_
#define MOVE_H_

#include <stdio.h>

typedef struct Position
{
	double x;
	double y;
	double z;
} Position;

typedef struct
{
	double ball_rotation;
	double ufo_rotation;
} Rotate;

typedef struct
{
	Position ball;
	Position grass;
	Position ufo;
} Move;

#endif