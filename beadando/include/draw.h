#ifndef DRAW_H
#define DRAW_H

#include "model.h"

void draw_model(const Model *model);

void draw_triangles(const Model *model);

void draw_quads(const Model *model);

void draw_normals(const Model *model, double length);

#endif