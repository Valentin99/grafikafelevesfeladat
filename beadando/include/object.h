#ifndef OBJECT_H_
#define OBJECT_H_
#include <stdio.h>
#include "entity.h"

#include "skybox.h"
#include "moveable_object.h"
#include "move.h"

typedef struct
{
    Entity grass;
    Entity tree;
    Entity table;
    Entity bench;
    Entity smalltree;
} Object;

void draw_environment(SkyBox SkyBox, Object Object, MoveableObject MoveableObject, Rotate *rotate, Move move);

#endif