#ifndef MODEL_H_
#define MODEL_H_

#include <stdio.h>

#include "entity.h"
#include "object.h"
#include "moveable_object.h"
#include "skybox.h"
#include "load.h"

#define TRUE 1
#define FALSE 0

void init_entities(SkyBox *SkyBox, Object *Object, MoveableObject *MoveableObject);

void init_model(Model* model);

void allocate_model(Model* model);

#endif
