#ifndef TRANSFORM_H_
#define TRANSFORM_H_

#include "entity.h"

void scale_model(Model *model, double sx, double sy, double sz);

#endif