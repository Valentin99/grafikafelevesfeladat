#ifndef INFO_H_
#define INFO_H_

#include "entity.h"

void print_model_info(const Model *model);

void print_bounding_box(const Model *model);

#endif