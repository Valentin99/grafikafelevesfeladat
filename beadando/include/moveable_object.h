#ifndef MOVEABLE_OBJECT_H_
#define MOVEABLE_OBJECT_H_
#include <stdio.h>

#include "entity.h"
#include "move.h"

typedef struct
{
    Entity ball;
    Entity ufo;
} MoveableObject;

void movement_of_objects(Move *move);

void rotation_of_objects(Rotate *rotate);

#endif