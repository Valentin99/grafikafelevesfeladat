#ifndef LOAD_H_
#define LOAD_H_

#include "entity.h"
#include <stdio.h>

#define INVALID_VERTEX_INDEX 0

struct TokenArray
{
    char **tokens;
    int n_tokens;
};

int load_model(const char *filename, Model *model);

void count_elements(FILE *file, Model *model);

void read_elements(FILE *file, Model *model);

void free_model(Model *model);

int is_numeric(char c);

void read_vertex(const struct TokenArray *token_array, struct Vertex *vertex);

void read_texture_vertex(const struct TokenArray *token_array, struct TextureVertex *texture_vertex);

void read_normal(const struct TokenArray *token_array, struct Vertex *normal);

void read_triangle(const struct TokenArray *token_array, struct Triangle *triangle);

void read_quad(const struct TokenArray *token_array, struct Quad *quad);

int count_tokens(const char *text);

int calc_token_length(const char *text, int start_index);

char *copy_token(const char *text, int offset, int length);

void insert_token(const char *token, struct TokenArray *token_array);

void extract_tokens(const char *text, struct TokenArray *token_array);

void free_tokens(struct TokenArray *token_array);

void init_model_counters(Model *model);

void clear_comment(char *line);

void count_element_in_line(const char *line, Model *model);

void read_element_from_line(const char *line, Model *model);

void read_face_point(const char *text, struct FacePoint *face_point);

int count_face_delimiters(const char *text);

int read_next_index(const char *text, int *length);

int is_valid_triangle(const struct Triangle *triangle, const Model *model);

int is_valid_quad(const struct Quad *quad, const Model *model);

#endif