#ifndef CALLBACKS_H
#define CALLBACKS_H

#include <sys/time.h>

#include "camera.h"
#include "init.h"
#include "texture.h"

struct {
	int mouse_x;
	int mouse_y;
} mouse_position;

void display();

void reshape(GLsizei width, GLsizei height);

void mouse_handler(int button, int state, int x, int y);

void motion_handler(int x, int y);

void key_up_handler(unsigned char key, int x, int y);

void key_handler(unsigned char key, int x, int y);

void idle();

void specialFunc(int key, int x, int y);

double calc_elapsed_time();

void update_camera_position(struct Camera *camera, double elapsed_time);

void fog();

void set_callbacks();

void draw_help();

#endif