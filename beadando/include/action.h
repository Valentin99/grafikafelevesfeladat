#ifndef ACTION_H_
#define ACTION_H_

#include <stdio.h>

struct Action
{
	int move_forward;
	int move_backward;
	int step_left;
	int step_right;
	int move_up;
	int move_down;
	int increase_light;
	int decrease_light;
};

#endif